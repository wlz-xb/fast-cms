/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50626
Source Host           : localhost:3306
Source Database       : fast-cms

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2017-06-15 16:07:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_code` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '菜单名称',
  `parent_menu_code` int(11) unsigned NOT NULL COMMENT '上级菜单名称',
  `href` varchar(128) COLLATE utf8_bin NOT NULL COMMENT '菜单链接 节点菜单没有 href',
  `target` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '页面打开方式 _blank',
  `icon` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '菜单图标',
  `sort` tinyint(11) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `display_flg` tinyint(4) unsigned NOT NULL DEFAULT '0' COMMENT '是否显示，0：不显示，1：显示',
  `permission` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '权限 对应shiro字符串',
  `remark` varchar(256) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` int(11) unsigned NOT NULL COMMENT '创建人',
  `update_by` int(11) unsigned NOT NULL COMMENT '更新人',
  `is_deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标志，0：未删除，1：已删除',
  `version` int(11)  NOT NULL DEFAULT '0' COMMENT 'version',
  PRIMARY KEY (`menu_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-菜单';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_menu_action`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu_action`;
CREATE TABLE `sys_menu_action` (
  `action_code` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '动作代码',
  `action_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `menu_code` int(11) unsigned NOT NULL COMMENT '菜单code',
  `href` varchar(256) COLLATE utf8_bin NOT NULL,
  `icon` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '图标',
  `sort` tinyint(11) unsigned NOT NULL COMMENT '排序',
  `display_flg` tinyint(4) unsigned DEFAULT '0' COMMENT '是否显示，0：不显示，1：显示',
  `permission` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '权限 对应shiro字符串',
  `remark` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` int(11) unsigned NOT NULL COMMENT '创建人',
  `update_by` int(11) unsigned NOT NULL COMMENT '更新人',
  `is_deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标志，0：未删除，1：已删除',
  `version` int(11)  NOT NULL DEFAULT '0' COMMENT 'version',
  PRIMARY KEY (`action_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-菜单-动作';

-- ----------------------------
-- Records of sys_menu_action
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_code` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '角色名称  管理员，普通用户，部门经理',
  `role_flag` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '角色标识，比如 admin user',
  `type` tinyint(4) unsigned NOT NULL COMMENT '角色类型，0：系统，1：微盟，2：代理商，3：供应商',
  `is_admin` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '管理员，0：非管理员，1：管理员',
  `data_scope` tinyint(4) unsigned NOT NULL DEFAULT '90' COMMENT '数据权限',
  `remark` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标志，0：未删除，1：已删除',
  `create_by` int(11) unsigned NOT NULL COMMENT '创建人',
  `update_by` int(11) unsigned NOT NULL COMMENT '更新人',
  `version` int(11)  NOT NULL DEFAULT '0' COMMENT 'version',
  PRIMARY KEY (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_action`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_action`;
CREATE TABLE `sys_role_action` (
  `role_code` int(11) unsigned NOT NULL COMMENT '角色Id',
  `action_code` int(11) unsigned NOT NULL COMMENT '菜单ID',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`role_code`,`action_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-角色-动作';

-- ----------------------------
-- Records of sys_role_action
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_code` int(11) unsigned NOT NULL COMMENT '角色Id',
  `menu_code` int(11) unsigned NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_code`,`menu_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-角色-菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `login_name` varchar(32) COLLATE utf8_bin NOT NULL COMMENT '登录名',
  `password` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '密码',
  `user_name` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '姓名',
  `email` varchar(64) COLLATE utf8_bin DEFAULT NULL COMMENT '电子邮件',
  `mobile` varchar(32) COLLATE utf8_bin DEFAULT NULL COMMENT '手机号码',
  `is_activated` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '账户激活标志，0：未冻结，1：冻结',
  `is_frozen` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT '账户冻结标志，0：未冻结，1：冻结',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `create_by` int(11) unsigned NOT NULL COMMENT '创建人',
  `update_by` int(11) unsigned NOT NULL COMMENT '更新人',
  `is_deleted` tinyint(2) unsigned NOT NULL DEFAULT '0' COMMENT '逻辑删除标志，0：未删除，1：已删除',
  `version` int(11)  NOT NULL DEFAULT '0' COMMENT 'version',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `udx_login_name` (`login_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_action`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_action`;
CREATE TABLE `sys_user_action` (
  `user_code` int(11) unsigned NOT NULL COMMENT '角色Id',
  `action_code` int(11) unsigned NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`user_code`,`action_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-用户-动作';

-- ----------------------------
-- Records of sys_user_action
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_menu`;
CREATE TABLE `sys_user_menu` (
  `user_code` int(11) unsigned NOT NULL COMMENT '角色Id',
  `menu_code` int(11) unsigned NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`user_code`,`menu_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-用户-菜单';

-- ----------------------------
-- Records of sys_user_menu
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_code` int(11) unsigned NOT NULL,
  `role_code` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_code`,`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='系统-用户-角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
