#fast-cms

# fast-cms 企业信息化快速开发平台

## 平台简介

Fast-cms致力于打造一套高可以的企业级开发脚手架。
Fast-cms是在Spring Framework基础上搭建的一个Java基础开发平台，模型视图控制器采用Spring MVC，数据访问层采用MyBatis，
权限授权采用Apache Shiro，数据本地缓存采用Ehcahe，页面模板引擎采用Thymeleaf；端前采用Seajs进行模板管理，弹出层方法采用
Layer，同时对前端通用js进行一定封装方便重用；后台页面借用了github上的Gentelella。

## 快速体验

1. 具备运行环境：JDK1.7+、Maven3.0+、MySql5+。
2. 修改src\main\resources\dev\application.properties文件中的数据库设置参数。
3. 根据修改参数创建对应MySql数据库用户和参数。
4. 将doc\中数据库文件执行。
5. 后台访问地址 http://localhost:8080/admin/login
6. 超级管理员 用户名：admin 密码：admin


## 如何交流、反馈、参与贡献？

* QQ 296282688  &nbsp; 
* E-mail：1013496155@qq.com
* GitHub：<https://github.com/Jakye>
* 开源中国：<https://git.oschina.net/0909>
* 支持Fast-cms发展：（加我好友）支付宝：1013496155@qq.com &nbsp; 微信：jackyechina