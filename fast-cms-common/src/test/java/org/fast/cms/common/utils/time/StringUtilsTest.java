package org.fast.cms.common.utils.time;

import org.fast.cms.common.utils.string.StringUtils;
import org.junit.Test;

import junit.framework.Assert;

/** 
* @author weigen.ye 
* @date 创建时间：2017年7月9日 下午12:57:17 
*
*/
public class StringUtilsTest {

	@Test
	public void startsWithIgnoreCaseTest(){
		String targetStr="abc";
		String prefixStr="A";
		Assert.assertEquals(true,StringUtils.startsWithIgnoreCase(targetStr, prefixStr));
		targetStr="Abc";
		prefixStr="a";
		Assert.assertEquals(true,StringUtils.startsWithIgnoreCase(targetStr, prefixStr));
		targetStr="\\Abc";
		prefixStr="\\a";
		Assert.assertEquals(true,StringUtils.startsWithIgnoreCase(targetStr, prefixStr));
		targetStr="/Abc";
		prefixStr="/a";
		Assert.assertEquals(true,StringUtils.startsWithIgnoreCase(targetStr, prefixStr));
	}
	
}

