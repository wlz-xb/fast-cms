package org.fast.cms.common.utils.time;

import java.util.Date;

import org.fast.cms.common.utils.time.DateUtils.HourMetric;
import org.junit.Test;

import junit.framework.Assert;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月16日 下午1:44:15 
*
*/
public class DateUtilsTest {
	
	@Test
	public void formatDateTest(){
		Date date=new Date();
		String dateStr=DateUtils.formatDate(date, "yyyy-MM-dd");
		Assert.assertEquals(dateStr,DateUtils.formatDate(new Date(), "yyyy-MM-dd"));
		System.out.println(DateUtils.formatDate(new Date(), "yM"));
		System.out.println(DateUtils.formatDate(new Date(), "23"));
	}
	
	@Test
	public void getSpecialHourOfDayTest(){
		String dateStr=DateUtils.formatDate(DateUtils.getSpecialHourOfDay(new Date(),25,HourMetric.HOUR_24),"yyyy-MM-dd HH:mm:ss");
		System.out.println(dateStr);
		dateStr=DateUtils.formatDate(DateUtils.getSpecialHourOfDay(new Date(),23,HourMetric.HOUR_24),"yyyy-MM-dd hh:mm:ss");
		System.out.println(dateStr);
		dateStr=DateUtils.formatDate(DateUtils.getSpecialHourOfDay(new Date(),11,HourMetric.HOUR_12),"yyyy-MM-dd HH:mm:ss");
		System.out.println(dateStr);
		Date date=DateUtils.getSpecialHourOfDay(new Date(),23,HourMetric.HOUR_12);
		System.out.println(date);
	}

	@Test
	public void parseDateTest(){
		System.out.println(DateUtils.parseDate("2017-09-09"));
	}
	
	@Test
	public void getMonthSpanTest(){
		Date begin=DateUtils.parseDate("2017-06-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
		Date end=DateUtils.parseDate("2018-07-30 23:59:59", "yyyy-MM-dd HH:mm:ss");
		System.out.println(DateUtils.getMonthSpan(begin, end));
		Assert.assertEquals(13, DateUtils.getMonthSpan(begin, end));
		begin=DateUtils.parseDate("2017-06-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
		end=DateUtils.parseDate("2017-06-30 23:59:59", "yyyy-MM-dd HH:mm:ss");
		System.out.println(DateUtils.getMonthSpan(begin, end));
	}
	
	@Test
	public void getFirstDayOfMonthTest(){
		Date date=DateUtils.parseDate("2017-06-17", "yyyyy-MM-dd");
		Assert.assertEquals("2017-06-01", DateUtils.formatDate(DateUtils.getFirstDayOfMonth(date), "yyyy-MM-dd"));
	}
	
	@Test
	public void getLastDayOfMonthTest(){
		Date date=DateUtils.parseDate("2020-01-10", "yyyyy-MM-dd");
		Assert.assertEquals("2020-01-31", DateUtils.formatDate(DateUtils.getLastDayOfMonth(date), "yyyy-MM-dd"));
		date=DateUtils.parseDate("2017-06-30", "yyyyy-MM-dd");
		Assert.assertEquals("2017-06-30", DateUtils.formatDate(DateUtils.getLastDayOfMonth(date), "yyyy-MM-dd"));
		date=DateUtils.parseDate("2017-06-01", "yyyyy-MM-dd");
		Assert.assertEquals("2017-06-30", DateUtils.formatDate(DateUtils.getLastDayOfMonth(date), "yyyy-MM-dd"));
		date=DateUtils.parseDate("2017-02-01", "yyyyy-MM-dd");
		Assert.assertEquals("2017-02-28", DateUtils.formatDate(DateUtils.getLastDayOfMonth(date), "yyyy-MM-dd"));
		date=DateUtils.parseDate("2020-02-10", "yyyyy-MM-dd");
		Assert.assertEquals("2020-02-29", DateUtils.formatDate(DateUtils.getLastDayOfMonth(date), "yyyy-MM-dd"));
	}
	
	@Test
	public void getFirstDayOfWeekTest(){
		//2017-06-18周六
		Date date=DateUtils.parseDate("2017-06-17", "yyyyy-MM-dd");
		Assert.assertEquals("2017-06-12", DateUtils.formatDate(DateUtils.getFirstDayOfWeek(date), "yyyy-MM-dd"));
		//2017-06-18周日
		date=DateUtils.parseDate("2017-06-18", "yyyyy-MM-dd");
		Assert.assertEquals("2017-06-12", DateUtils.formatDate(DateUtils.getFirstDayOfWeek(date), "yyyy-MM-dd"));
		//2017-06-12周一
		date=DateUtils.parseDate("2017-06-12", "yyyyy-MM-dd");
		Assert.assertEquals("2017-06-12", DateUtils.formatDate(DateUtils.getFirstDayOfWeek(date), "yyyy-MM-dd"));
	}
	
	@Test
	public void getDateTest(){
		Assert.assertEquals("2017-06-12", DateUtils.formatDate(DateUtils.getDate(20170612), "yyyy-MM-dd"));
		Assert.assertEquals("2017-01-10", DateUtils.formatDate(DateUtils.getDate(20170110), "yyyy-MM-dd"));
		Assert.assertEquals("2000-01-10", DateUtils.formatDate(DateUtils.getDate(20000110), "yyyy-MM-dd"));
	}
	
	@Test
	public void getRoundedHourDateTest(){
		Date date=DateUtils.parseDate("2017-06-12 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 23:00:00", DateUtils.formatDate(DateUtils.getRoundedHourDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-06-12 23:00:01", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 23:00:00", DateUtils.formatDate(DateUtils.getRoundedHourDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-06-12 23:01:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 23:00:00", DateUtils.formatDate(DateUtils.getRoundedHourDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-06-12 23:00:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 23:00:00", DateUtils.formatDate(DateUtils.getRoundedHourDate(date), "yyyy-MM-dd HH:mm:ss"));
	}
	
	@Test
	public void getStartOfDateTest(){
		Date date=DateUtils.parseDate("2017-06-12 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 00:00:00", DateUtils.formatDate(DateUtils.getStartOfDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-08-12 23:00:01", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-08-12 00:00:00", DateUtils.formatDate(DateUtils.getStartOfDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-01-01 23:01:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-01-01 00:00:00", DateUtils.formatDate(DateUtils.getStartOfDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-06-12 23:00:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 00:00:00", DateUtils.formatDate(DateUtils.getStartOfDate(date), "yyyy-MM-dd HH:mm:ss"));
	}
	
	@Test
	public void getEndOfDateTest(){
		Date date=DateUtils.parseDate("2017-06-12 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 23:59:59", DateUtils.formatDate(DateUtils.getEndOfDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-08-12 23:00:01", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-08-12 23:59:59", DateUtils.formatDate(DateUtils.getEndOfDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-01-01 23:01:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-01-01 23:59:59", DateUtils.formatDate(DateUtils.getEndOfDate(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-06-12 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-12 23:59:59", DateUtils.formatDate(DateUtils.getEndOfDate(date), "yyyy-MM-dd HH:mm:ss"));
	}
	
	@Test
	public void getNextDayBeginTime(){
		Date date=DateUtils.parseDate("2017-06-12 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-13 00:00:00", DateUtils.formatDate(DateUtils.getNextDayBeginTime(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-01-31 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-02-01 00:00:00", DateUtils.formatDate(DateUtils.getNextDayBeginTime(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-01-31 23:00:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-02-01 00:00:00", DateUtils.formatDate(DateUtils.getNextDayBeginTime(date), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-12-31 23:00:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2018-01-01 00:00:00", DateUtils.formatDate(DateUtils.getNextDayBeginTime(date), "yyyy-MM-dd HH:mm:ss"));
	}
	
	@Test
	public void getNextDayEndTimeTset(){
		Date date=DateUtils.parseDate("2017-06-12 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-13 23:59:59", DateUtils.formatDate(DateUtils.getNextDayEndTime(date), "yyyy-MM-dd HH:mm:ss"));
		System.out.println(DateUtils.getNextDayEndTime(date).getTime());
		date=DateUtils.parseDate("2017-06-12 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-13 23:59:59", DateUtils.formatDate(DateUtils.getNextDayEndTime(date), "yyyy-MM-dd HH:mm:ss"));
		System.out.println(DateUtils.getNextDayEndTime(date).getTime());
		date=DateUtils.parseDate("2017-01-31 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-02-01 23:59:59", DateUtils.formatDate(DateUtils.getNextDayEndTime(date), "yyyy-MM-dd HH:mm:ss"));
		System.out.println(DateUtils.getNextDayEndTime(date).getTime());
		date=DateUtils.parseDate("2017-01-31 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-02-01 23:59:59", DateUtils.formatDate(DateUtils.getNextDayEndTime(date), "yyyy-MM-dd HH:mm:ss"));
		System.out.println(DateUtils.getNextDayEndTime(date).getTime());
		date=DateUtils.parseDate("2017-12-31 23:00:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2018-01-01 23:59:59", DateUtils.formatDate(DateUtils.getNextDayEndTime(date), "yyyy-MM-dd HH:mm:ss"));
		System.out.println(DateUtils.getNextDayEndTime(date).getTime());
	}
	
	@Test
	public void getSpacialDayBeginTimeTest(){
		Date date=DateUtils.parseDate("2017-06-12 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-06-13 00:00:00", DateUtils.formatDate(DateUtils.getSpacialDayBeginTime(date,1), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-06-12 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2018-06-12 00:00:00", DateUtils.formatDate(DateUtils.getSpacialDayBeginTime(date,365), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-06-12 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2018-06-12 00:00:00", DateUtils.formatDate(DateUtils.getSpacialDayBeginTime(date,365), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2017-01-31 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2017-03-02 00:00:00", DateUtils.formatDate(DateUtils.getSpacialDayBeginTime(date,30), "yyyy-MM-dd HH:mm:ss"));
		date=DateUtils.parseDate("2020-01-31 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals("2020-03-01 00:00:00", DateUtils.formatDate(DateUtils.getSpacialDayBeginTime(date,30), "yyyy-MM-dd HH:mm:ss"));
	}
	
	@Test
	public void getBetweenDateTest(){
		Date startDate=DateUtils.parseDate("2017-06-12 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		Date endDate=DateUtils.parseDate("2017-06-13 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(1L,DateUtils.getBetweenDate(startDate, endDate));
		startDate=DateUtils.parseDate("2017-02-01 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		endDate=DateUtils.parseDate("2017-03-01 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(28L,DateUtils.getBetweenDate(startDate, endDate));
		startDate=DateUtils.parseDate("2020-02-01 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		endDate=DateUtils.parseDate("2020-03-01 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(29L,DateUtils.getBetweenDate(startDate, endDate));
	}
	
	@Test
	public void getDaySpanTest(){
		Date startDate=DateUtils.parseDate("2017-06-12 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		Date endDate=DateUtils.parseDate("2017-06-13 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(1L,DateUtils.getDaySpan(startDate, endDate));
		startDate=DateUtils.parseDate("2017-02-01 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		endDate=DateUtils.parseDate("2017-03-01 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(28L,DateUtils.getDaySpan(startDate, endDate));
		startDate=DateUtils.parseDate("2020-02-01 00:00:00", "yyyyy-MM-dd HH:mm:ss");
		endDate=DateUtils.parseDate("2020-03-01 23:59:59", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(29L,DateUtils.getDaySpan(startDate, endDate));
	}
	

	@Test
	public void validateDateStringTest(){
		Assert.assertEquals(true,DateUtils.validateDateString("20200229"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000131"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000229"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000331"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000430"));
		Assert.assertEquals(false,DateUtils.validateDateString("20000431"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000531"));
		Assert.assertEquals(false,DateUtils.validateDateString("20000631"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000630"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000731"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000831"));
		Assert.assertEquals(false,DateUtils.validateDateString("20000931"));
		Assert.assertEquals(true,DateUtils.validateDateString("20000930"));
		Assert.assertEquals(true,DateUtils.validateDateString("20001031"));
		Assert.assertEquals(false,DateUtils.validateDateString("20001131"));
		Assert.assertEquals(true,DateUtils.validateDateString("20001130"));
		Assert.assertEquals(true,DateUtils.validateDateString("20001231"));
		
		Assert.assertEquals(true,DateUtils.validateDateString("19990131"));
		Assert.assertEquals(false,DateUtils.validateDateString("19990229"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990228"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990331"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990430"));
		Assert.assertEquals(false,DateUtils.validateDateString("19990431"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990531"));
		Assert.assertEquals(false,DateUtils.validateDateString("19990631"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990630"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990731"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990831"));
		Assert.assertEquals(false,DateUtils.validateDateString("19990931"));
		Assert.assertEquals(true,DateUtils.validateDateString("19990930"));
		Assert.assertEquals(true,DateUtils.validateDateString("19991031"));
		Assert.assertEquals(false,DateUtils.validateDateString("19991131"));
		Assert.assertEquals(true,DateUtils.validateDateString("19991130"));
		Assert.assertEquals(true,DateUtils.validateDateString("19991231"));
		Assert.assertEquals(true,DateUtils.validateDateString("09991231"));
		Assert.assertEquals(false,DateUtils.validateDateString("19991331"));
		Assert.assertEquals(false,DateUtils.validateDateString("19991232"));
	}
	
	@Test
	public void isBetweenDateTest(){
		Date startDate=DateUtils.parseDate("2017-06-12 01:00:00", "yyyyy-MM-dd HH:mm:ss");
		Date endDate=DateUtils.parseDate("2017-06-12 02:00:00", "yyyyy-MM-dd HH:mm:ss");
		Date specialDate=DateUtils.parseDate("2017-06-12 01:59:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(true,DateUtils.isBetweenDate(startDate,endDate,specialDate));
		Assert.assertEquals(true,DateUtils.isBetweenDate(startDate,endDate,specialDate));
		
		startDate=DateUtils.parseDate("2017-06-12 01:00:00", "yyyyy-MM-dd HH:mm:ss");
		endDate=DateUtils.parseDate("2017-06-12 02:00:00", "yyyyy-MM-dd HH:mm:ss");
		specialDate=DateUtils.parseDate("2017-06-12 03:59:00", "yyyyy-MM-dd HH:mm:ss");
		Assert.assertEquals(true,DateUtils.isBetweenDate(startDate,endDate,specialDate,false));
		Assert.assertEquals(false,DateUtils.isBetweenDate(startDate,endDate,specialDate));
	}
	
}
