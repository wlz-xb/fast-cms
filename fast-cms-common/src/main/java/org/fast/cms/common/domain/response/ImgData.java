package org.fast.cms.common.domain.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月18日 上午7:21:51 
*
*/
@SuppressWarnings("serial")
@Getter
@Setter
public class ImgData implements Serializable {
	
	private String type;
	
	private String base64;
	
	private String cosUrl;

}
