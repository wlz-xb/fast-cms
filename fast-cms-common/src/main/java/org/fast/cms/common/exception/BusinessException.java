package org.fast.cms.common.exception;

import org.fast.cms.common.domain.support.MessageInfo;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@SuppressWarnings("serial")
public class BusinessException extends RuntimeException {

    private String errorCode;

    private String message;

    public BusinessException(String message) {
        super(message);
        this.message = message;
    }

    public BusinessException(MessageInfo msg) {
        super(msg.getMessage());
        this.message = msg.getMessage();
        this.errorCode = msg.getCode();
    }

    public BusinessException(String message, Throwable e) {
        super(message, e);
        this.message = message;
    }
}
