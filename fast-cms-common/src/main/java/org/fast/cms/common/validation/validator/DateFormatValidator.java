package org.fast.cms.common.validation.validator;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.fast.cms.common.utils.string.StringUtils;
import org.fast.cms.common.utils.time.DateUtils;
import org.fast.cms.common.validation.anno.DateFormat;

public class DateFormatValidator implements ConstraintValidator<DateFormat, String> {

    private String dateFormat;

    @Override
    public void initialize(DateFormat constraintAnnotation) {
    	
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isBlank(value)) return true;
        Date result=DateUtils.parseDate(value, dateFormat);
        if(result==null){
        	return false;
        }
        return true;
    }

}
