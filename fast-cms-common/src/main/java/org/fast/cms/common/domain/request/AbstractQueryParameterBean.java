package org.fast.cms.common.domain.request;

import javax.validation.constraints.Pattern;

import com.alibaba.druid.util.StringUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public abstract class AbstractQueryParameterBean extends AbstractBaseBean {

	/** 每页大小 */
	private Integer pageSize;

	/** 当前页码 */
	private Integer pageNum;

	/** 排序字段 */
	@Pattern(regexp="(^\\w+)", message="排序字段不正确")
	private String sortKey;

	/** 排序方向（1/NULL--正序， 2--逆序） */
	private Integer sortAsc;
	
	/**
	 * 排序字符串
	 */
	private String sortStr;
	
	/** 当前用户ID（数据权限用） */
	private Integer userId;

	/** 当前用户部门（数据权限用） */
	private Integer deptCode;
	
	/** 数据权限SQL */
	private String dataAuthoritySql;
	
	/** 数据权限范围 */
	private int dataScope;
	
	public String getSortStr() {
		if (!StringUtils.isEmpty(this.sortKey)) {
			if (this.sortAsc != null && this.sortAsc.intValue() == 2) {
				sortStr = sortKey + " desc";
			} else {
				sortStr = sortKey;
			}
		}
		return sortStr;
	}
	
	public void setSortStr(String sortStr) {
		this.sortStr = sortStr;
	}
}
