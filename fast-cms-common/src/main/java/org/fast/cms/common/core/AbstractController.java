package org.fast.cms.common.core;

import org.fast.cms.common.domain.response.JsonResponse;
import org.fast.cms.common.domain.support.MessageInfo;
import org.fast.cms.common.enums.MessageCodeEnum;
import org.fast.cms.common.utils.MessageUtils;
import org.fast.cms.common.utils.string.StringUtils;
import org.springframework.web.servlet.ModelAndView;

/** 
* 
* 响应请求基类
* @author weigen.ye 
* @date 创建时间：2017年6月15日 下午8:44:39 
*
*/
public abstract class AbstractController implements SuperController{
	
	
	protected ModelAndView getRedirectModelAndView(ModelAndView view, String viewName, boolean isAadminTemplate, Boolean isAbsolute) {
	     view.setViewName(this.getRedirectTemplate(viewName, isAadminTemplate, isAbsolute));
	     return view;
	}
	
	protected ModelAndView getRedirectModelAndView(String viewName, Boolean isAadminTemplate, Boolean isAbsolute) {
		 ModelAndView view=new ModelAndView();
		 view.setViewName(this.getRedirectTemplate(viewName, isAadminTemplate, isAbsolute));
	     return view;
	}
	
	/**
	 * 根据模板路径重定向到模板
	 * @param path 模板路径
	 * @param isAadminTemplate 是否为后台模板为null时为false
	 * @param isAbsolute 是否为绝对路径，为null时为false
	 * @return
	 */
	protected String getRedirectTemplate(String path , Boolean isAadminTemplate, Boolean isAbsolute){
		//去除path中的redirect:前缀
		path=this.deleteRedirectPrefix(path);
		String result="";
		isAadminTemplate=isAadminTemplate==null?false:isAadminTemplate;
		isAbsolute=isAbsolute==null?false:isAbsolute;
		result=isAadminTemplate?this.getAdminTemplate(path):this.getFrontTemplate(path);
//		result=deleteFirstSlash(path);
		result=getRedirectTemplat(result,isAbsolute);
		return result;
	}
	
	/**
	 * 获取后台的模板路径
	 * @param path 模板路径
	 * @return
	 */
	protected String getAdminTemplate(String path){
		return ADMIN_TEMPLATE+this.deleteFirstSlash(path);
	}
	
	/**
	 * 获取前台的模板路径
	 * @param path 模板路径
	 * @return
	 */
	protected String getFrontTemplate(String path){
		return FRONT_TEMPLATE+this.deleteFirstSlash(path);
	}
	
	/**
	 * 根据path得到相应的重定向路径
	 * @param path 路径
	 * @param isAbsolute 是否为绝对路径
	 * @return
	 */
	private String getRedirectTemplat(String path , Boolean isAbsolute){
		String result="";
		result=isAbsolute?SLASH+path:path;
		return result;
	}
	
	/**
	 * 去除path中的第一个斜杠
	 * @param path 路径
	 * @return
	 */
	private String deleteFirstSlash(String path){
		String result=path==null?"":path;
		if(path==null||!path.startsWith("/")){
			return result;
		}
		result=path.substring(1);
		return result;
	}
	
	/**
	 * 去除path中的redirect:前缀
	 * @param path 路径
	 * @return
	 */
	private String deleteRedirectPrefix(String path){
		String result=path==null?"":path;
		if(path==null||!StringUtils.startsWithIgnoreCase(path, REDIRECT)){
			return result;
		}
		result=path.substring(REDIRECT.length());
		return result;
	}
	
	/**
	 * 请求成功响应，有code，无message，无data
	 * @return
	 */
	protected  JsonResponse<Void>  success() {
		return success(null,null);
	}
	
	/**
	 * 请求成功响应
	 * @param data 响应数据
	 * @return
	 */
	protected  <T> JsonResponse<T> success(T data) {
		return success(data,null);
	}
	
	/**
	 * 请求成功响应
	 * @param data 响应数据
	 * @param message 响应消息内容
	 * @return
	 */
	protected <T> JsonResponse<T>  success(T data, String  message) {
		MessageInfo info=MessageUtils.getLocaleMessageInfo(MessageCodeEnum.SUCCESS);
		JsonResponse<T>  j = new JsonResponse<T>(info,data);
		if(message!=null){
			j.setMessage(message);
		}
		return j;
	}
	
	/**
	 * 请求失败响应
	 * @param message 响应消息内容
	 * @return
	 */
	protected <T>  JsonResponse<T> error(String message){
		return error(null, MessageCodeEnum.FAILED,message);
	}
	
	/**
	 * 请求失败响应 
	 * @param codeEnum 错误编码
	 * @return
	 */
	protected <T>  JsonResponse<T> error(MessageCodeEnum codeEnum){
		return error(null, codeEnum,null);
	}
	
	/**
	 * 请求失败响应 
	 * @param codeEnum 错误编码
	 * @param message 响应消息内容
	 * @return
	 */
	protected <T>  JsonResponse<T> error(MessageCodeEnum codeEnum,String message){
		return error(null, codeEnum, message);
	}
	
	/**
	 * 请求失败响应 
	 * @param data 响应数据
	 * @param codeEnum 错误编码
	 * @param message 响应消息内容
	 * @return
	 */
	protected <T>  JsonResponse<T> error(T data, MessageCodeEnum codeEnum, String message){
		MessageInfo info=MessageUtils.getLocaleMessageInfo(codeEnum);
		JsonResponse<T>  j = new JsonResponse<T>(info,data);
		if(message!=null){
			j.setMessage(message);
		}
		return j;
	}
}
