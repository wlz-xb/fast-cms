package org.fast.cms.common.web.aspect;

import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.InputStreamSource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;

@Aspect
@Component
@Order(1)
public class WebLogAspect {

	private static Logger log = LoggerFactory.getLogger("performance");

	@Pointcut("execution(public * com.fast.cms..web..*.*(..))")
	public void webLog() {
		
	}

	@Around("webLog()")
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		long start = System.currentTimeMillis();
		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
		List<Object> args = Lists.newArrayList();
		try {
			for(Object o : pjp.getArgs()) {
				if (o instanceof ServletResponse
						|| o instanceof ServletRequest
						|| o instanceof Exception
						|| o instanceof InputStreamSource) {
					continue;
				}
				args.add(o);
			}
			log.info("========================");
			log.info("URL : " + request.getRequestURL().toString());
			log.info("HTTP_METHOD : " + request.getMethod());
	        log.info("IP : " + request.getRemoteAddr());
	        log.info("CLASS_METHOD : " + pjp.getSignature().getDeclaringTypeName() + "." + pjp.getSignature().getName());
	        log.info("ARGS : " + JSON.toJSONString(args, true));
			return pjp.proceed();
		} finally {
			long last = System.currentTimeMillis() - start;
		    log.info(" execution: " + last + "ms.");		   
		}
	}
}
