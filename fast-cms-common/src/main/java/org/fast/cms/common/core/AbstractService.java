package org.fast.cms.common.core;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.github.pagehelper.Page;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月15日 下午8:31:30 
*
*/
public abstract class AbstractService implements SuperService{

	public <T> T fisrtOne(List<T> list) {
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}
	
	public <T> Page<T> buildPage(List<T> list) {
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		if (!(list instanceof Page)) {
			Page<T> page = new Page<T>();
			page.addAll(list);
			return page;
		}
		return (Page<T>) list;
	}
}
