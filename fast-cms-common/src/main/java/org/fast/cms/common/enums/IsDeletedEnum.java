package org.fast.cms.common.enums;

public enum IsDeletedEnum {
	NORMAL(0,"正常"),
	DELETE(1,"删除");
    private final Integer key;
    private final String  value;

    private IsDeletedEnum(final Integer key,final String value) {
        this.key = key;
        this.value = value;
    }

    public static IsDeletedEnum valueOf(int key) {
        for (IsDeletedEnum num : values()) {
            if (num.getKey() == key) { 
                return num;
            }
        }
        return null;
    }

	public Integer getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}
	
}

