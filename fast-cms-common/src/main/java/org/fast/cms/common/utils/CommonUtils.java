package org.fast.cms.common.utils;

import org.fast.cms.common.domain.response.AbstractPageBean;
import com.github.pagehelper.Page;

public class CommonUtils {

	/**
	 * 设置分页信息
	 * @param bean
	 * @param page
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void setPageInfo(AbstractPageBean bean, Page page) {
		bean.setList(page.getResult());
		bean.setPageNum(page.getPageNum());
		bean.setPages(page.getPages());
		bean.setPageSize(page.getPageSize());
		bean.setTotal(page.getTotal());
	}
}
