package org.fast.cms.common.utils;

import java.util.Locale;

import org.fast.cms.common.domain.support.MessageInfo;
import org.fast.cms.common.enums.MessageCodeEnum;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;

/** 
* 读取国际化消息内容
* @author weigen.ye 
* @date 创建时间：2017年6月18日 上午7:29:35 
*/
@Component
public class MessageUtils implements MessageSourceAware {

    private static MessageSourceAccessor messageSourceAccessor;

    @Override
    public void setMessageSource(MessageSource messageSource) {
        messageSourceAccessor = new MessageSourceAccessor(messageSource, Locale.CHINA);
    }

    public static String getLocaleMessage(MessageCodeEnum code) {
        return getLocaleMessage(code, new Object[] {});
    }

    public static String getLocaleMessage(MessageCodeEnum code, Object... args) {
        return messageSourceAccessor.getMessage(code.getCode(), args);
    }

    public static MessageInfo getLocaleMessageInfo(MessageCodeEnum code) {
        return getLocaleMessageInfo(code, new Object[] {});
    }

    public static MessageInfo getLocaleMessageInfo(MessageCodeEnum code, Object... args) {
        return new MessageInfo(code.getCode(), getLocaleMessage(code, args));
    }
    
    public static MessageInfo success() {
    	return new MessageInfo(MessageCodeEnum.SUCCESS.getCode(), getLocaleMessage(MessageCodeEnum.SUCCESS));
    }
}
