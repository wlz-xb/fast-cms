package org.fast.cms.common.domain.response;

import lombok.Getter;
import lombok.Setter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月18日 上午7:24:29 
*
*/
@Getter
@Setter
@SuppressWarnings("serial")
public class ValidateCodeData extends ImgData {

	String validateCode;
}