package org.fast.cms.common.web.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Component
@Order(1)
public class ServiceAspect {

	@Around("execution(public * com.fast.cms..service.*.impl.*(..))")
	public Object around(ProceedingJoinPoint pjp) throws Throwable {
		long startTime = System.currentTimeMillis();
		MethodSignature method = (MethodSignature) pjp.getSignature();
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(method.getDeclaringTypeName());
		sb.append(".");
		sb.append(method.getName());
		sb.append("]");
		sb.append(".Start");
		try {
			log.info(sb.toString());
			return pjp.proceed();
		} finally {
			sb = new StringBuilder();
			sb.append("[");
			sb.append(method.getDeclaringTypeName());
			sb.append(".");
			sb.append(method.getName());
			sb.append("]");
			sb.append(".End");
			sb.append("  Time:");
			sb.append(System.currentTimeMillis() - startTime);
			sb.append("ms");
			log.info(sb.toString());
		}
	}
}
