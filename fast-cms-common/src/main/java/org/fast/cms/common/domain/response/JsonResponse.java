package org.fast.cms.common.domain.response;

import java.io.Serializable;

import org.fast.cms.common.domain.support.MessageInfo;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@SuppressWarnings("serial")
public class JsonResponse<T> implements Serializable {
	
	/**
	 * 状态码
	 */
	private String code;

	/**
	 * 消息内容
	 */
	private String message;

	/**
	 * 返回数据
	 */
	private T data;
	
	public JsonResponse() {
		
	}
	
	public JsonResponse(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public JsonResponse(String code, String message, T data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public JsonResponse(MessageInfo m) {
		this(m, null);
	}

	public JsonResponse(MessageInfo m, T data) {
		this(m.getCode(), m.getMessage(), data);
	}
}
