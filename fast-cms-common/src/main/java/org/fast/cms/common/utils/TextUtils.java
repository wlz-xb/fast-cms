package org.fast.cms.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月23日 下午4:41:57 
*
*/
final public class TextUtils {
	
	private static final String  INPUT_FILE_FATH="C:\\Users\\lenovo\\Desktop\\test.txt";
	
	private static final String  OUT_FILE_FATH="C:\\Users\\lenovo\\Desktop\\result.txt";
	
	private static final String[] fliterNum={"15","18","33","274","286","321","358","361","370","371","376",
			 "377","441","461","555","603","604","612","1115","1180","1289","1800","1827"};

	public static void changeEscapeToComma(boolean fliterFlag){
        try {
        	File inputFile=new File(INPUT_FILE_FATH);
        	File outputFile=new File(OUT_FILE_FATH);
//	        String encoding="UTF-8";
        	//判断文件是否存在
            if(inputFile.isFile() && inputFile.exists()){ 
            	InputStreamReader read = new InputStreamReader( new FileInputStream(inputFile));//考虑到编码格式
            	BufferedReader bufferedReader = new BufferedReader(read);
                String record = null;
                StringBuilder temp=new StringBuilder("(");
                boolean contianFlag=false;
                while((record = bufferedReader.readLine()) != null){
                	contianFlag=false;
                	if(fliterFlag){
                		for(String num:fliterNum){
                    		if(num.equals(record)){
                    			contianFlag=true;
                    			break;
                    		}
                    	}
                	}
                	if(!contianFlag){
                		temp.append(record).append(",");
                	}
                }
                String result=temp.substring(0, temp.length()-1);
                result=result+")";
                read.close();
                if(!outputFile.exists()){
                	outputFile.createNewFile();
                }
                FileWriter  fileWriter=new FileWriter(outputFile);
                fileWriter.write(result);
                fileWriter.close();
            }else{
                System.out.println("找不到指定的文件");
            }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
        System.out.println("转换成功！");
    }
	
	public static void countDistinctNum(){
		 try {  
			    int n=0; 
	        	File inputFile=new File(INPUT_FILE_FATH);
	        	Map<String,String> map=new HashMap<>();
//		        String encoding="UTF-8";
	        	//判断文件是否存在
	            if(inputFile.isFile() && inputFile.exists()){ 
	            	InputStreamReader read = new InputStreamReader( new FileInputStream(inputFile));//考虑到编码格式
	            	BufferedReader bufferedReader = new BufferedReader(read);
	                String record = null;
	                while((record = bufferedReader.readLine()) != null){
	                	if(!map.containsKey(record)){
	                		n++;
	                		map.put(record, record);
	                	}
	                }
	                read.close();
	                System.out.println("一共："+n+"条记录！");
	            }else{
	                System.out.println("找不到指定的文件");
	            }
	        } catch (Exception e) {
	            System.out.println("读取文件内容出错");
	            e.printStackTrace();
	        }
	}
	
	public static void main(String[] args){
		changeEscapeToComma(false);
//		countDistinctNum();
	}
}
