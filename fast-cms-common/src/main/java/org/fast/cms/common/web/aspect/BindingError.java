package org.fast.cms.common.web.aspect;

import lombok.Data;

import java.io.Serializable;


@Data
@SuppressWarnings("serial")
public class BindingError implements Serializable {

    /**
     *  消息内容
     */
    private String message;

    /**
     *  字段名
     */
    private String name;
}
