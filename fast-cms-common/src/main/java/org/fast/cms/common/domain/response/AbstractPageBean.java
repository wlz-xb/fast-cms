package org.fast.cms.common.domain.response;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class AbstractPageBean<T> implements Serializable {
	/**
     * 页码，从1开始
     */
    private int pageNum;
    
	/**
     * 页面大小
     */
    private int pageSize;
    
	/**
     * 总数
     */
    private long total;
    
	/**
     * 总页数
     */
    private int pages;
    
    /**
     * 返回结果
     */
    private List<T> list;
	
}
