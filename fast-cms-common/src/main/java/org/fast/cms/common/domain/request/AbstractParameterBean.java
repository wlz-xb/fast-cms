package org.fast.cms.common.domain.request;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import net.sf.jsqlparser.statement.update.Update;

@Getter
@Setter
@SuppressWarnings("serial")
public abstract class AbstractParameterBean extends AbstractBaseBean {

	@NotNull(groups={Update.class}, message="数据版本号")
	private Integer version;
}
