package org.fast.cms.common.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.util.DigestUtils;

/**
 * Created by lenovo on 2017/1/22.
 */
public class CryptUtils {

	private static final String UTF_16LE = "UTF-16LE";

	private static final String SecretKey = "DESede";

	private static final String Algorithm = "DESede/ECB/PKCS5Padding";

	public final static String MD5(String s) {
        char hexDigits[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};       
        try {
            byte[] btInput = s.getBytes();
            // 获得MD5摘要算法的 MessageDigest 对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            // 使用指定的字节更新摘要
            mdInst.update(btInput);
            // 获得密文
            byte[] md = mdInst.digest();
            // 把密文转换成十六进制的字符串形式
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	
	/**
	 * 自定义一个key
	 * 
	 * @param string
	 */
	public static SecretKey getKey(String keyRule) throws UnsupportedEncodingException {
		SecretKey key = null;
		byte[] tmp = keyRule.getBytes("ASCII");

		byte[] keyByte = DigestUtils.md5Digest(tmp);
		// 创建一个空的八位数组,默认情况下为0
		byte[] byteTemp = new byte[24];
		// 将用户指定的规则转换成八位数组
		for (int i = 0; i < byteTemp.length && i < keyByte.length; i++) {
			byteTemp[i] = keyByte[i];
		}
		key = new SecretKeySpec(byteTemp, SecretKey);
		return key;
	}

	/**
	 * 3DESECB加密
	 * 
	 * @param src
	 *            要进行了加密的原文
	 * @param key
	 *            密钥 key必须是长度大于等于 3*8 = 24 位
	 * @return
	 * @throws Exception
	 */
	public static String encryptMode(String src, String keybyte) {
		try {
			// 生成密钥
			SecretKey deskey = getKey(keybyte);
			// 加密
			Cipher c1 = Cipher.getInstance(Algorithm);
			c1.init(Cipher.ENCRYPT_MODE, deskey);
			byte [] tmp = c1.doFinal(src.getBytes(UTF_16LE));
			return Encodes.encodeBase64(tmp);
		} catch (java.security.NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		} catch (javax.crypto.NoSuchPaddingException e2) {
			e2.printStackTrace();
		} catch (java.lang.Exception e3) {
			e3.printStackTrace();
		}
		return null;
	}

	/**
	 * 3DESECB解密
	 * 
	 * @param src
	 *            要解密的密文字符
	 * @param key
	 *            解密的Key key必须是长度大于等于 3*8 = 24 位
	 * @return
	 * @throws Exception
	 */
	public static String decryptDES(String src, String key) throws Exception {
		// --通过base64,将字符串转成byte数组
		byte[] bytesrc = Encodes.decodeBase64(src);
		// --解密的key
		SecretKey securekey = getKey(key);

		// --Chipher对象解密
		Cipher cipher = Cipher.getInstance(Algorithm);
		cipher.init(Cipher.DECRYPT_MODE, securekey);
		byte[] retByte = cipher.doFinal(bytesrc);

		return new String(retByte, UTF_16LE);
	}

	public static void main(String[] args) throws Exception {
//		String clearText = "890"; // 这里的数据长度必须为8的倍数
		String key = "Password1@ETA";
//		System.out.println("明文：" + clearText + "\n密钥：" + key);
//		String encryptText = encryptMode(clearText, key);
//		System.out.println("加密后：" + encryptText);
//		String decryptText = decryptDES(encryptText, key);
//		System.out.println("解密后：" + decryptText);
//		System.out.println("解密后：" + decryptDES("gz8IqhO++RQ=", key));
		
		System.out.println(decryptDES("3Au5FM8p8cs=", key));
	}
}
