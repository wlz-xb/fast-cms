package org.fast.cms.common.enums;

import lombok.Getter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月18日 上午8:27:22 
*
*/
@Getter
public enum MessageCodeEnum {
    
	SUCCESS("0"),
    FAILED("1"),
    // 缺少参数
    MISSING_PARAM("4000006"),
    // 无效参数
    INVAID_PARAM("4000007"),
    // 参数验证错误
    PARAM_VALID_ERROR("4000008"),
    //没有权限查看该数据
    DATA_NO_AUTHORITY("4000088"),
    //数据的状态不适合该操作
    DATA_OPERATE_FAILED("4000089"),
    //没有数据版本信息
    NO_VERSION("4000091"),
    // 没有分页信息
    NO_PAGE_INFO("4000092"),
    // 该数据被他人更新
    UPDATE_CONFLICT("4000093"),
    // 未登录
    NOT_LOGIN("4000097"),
    // 未受权
    UN_AUTHORIZED("4000098"),
    //未知异常
    UNKNOWN_ERROR("4000099"),
    //认证失败
	AUTHETICATION_FAILED("4000094");
	
	public String code;
	
	MessageCodeEnum(String code){
		this.code=code;
	}
}
