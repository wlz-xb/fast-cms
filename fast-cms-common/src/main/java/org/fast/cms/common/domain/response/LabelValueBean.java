package org.fast.cms.common.domain.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by junxiao on 2017/2/14.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("serial")
public class LabelValueBean implements Serializable {

    /**
     * 值
     */
    private Integer value;

    /**
     * 表示内容
     */
    private String label;
    
    /**
     * str值
     */
    private String valueStr;
    
	public LabelValueBean(Integer value, String label) {
		this.value = value;
		this.label = label;
	}
}
