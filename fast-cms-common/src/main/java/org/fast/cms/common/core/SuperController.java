package org.fast.cms.common.core;
/** 
* 所有Controller一级接口
* @author weigen.ye 
* @date 创建时间：2017年6月15日 下午8:43:41 
*
*/
public interface SuperController {
	
	//重定向
	public static final String REDIRECT="redirect:";
	//斜杠
	public static final String SLASH="/";
	//后台模板路径
	public static final String ADMIN_TEMPLATE="admin/";
	//前台模板路径
	public static final String FRONT_TEMPLATE="front/";
}
