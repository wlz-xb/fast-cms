package org.fast.cms.common.exception;

import org.fast.cms.common.enums.MessageCodeEnum;
import org.fast.cms.common.utils.MessageUtils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
public class UpdateConflictException extends RuntimeException {
    
	private String msg;

    public UpdateConflictException() {
        super(MessageUtils.getLocaleMessage(MessageCodeEnum.UPDATE_CONFLICT));
        msg = MessageUtils.getLocaleMessage(MessageCodeEnum.UPDATE_CONFLICT);
    }

    public UpdateConflictException(String msg) {
        super(msg);
        this.msg = msg;
    }
    
}
