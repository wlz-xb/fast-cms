package org.fast.cms.common.exception;

/**
 * Created by lenovo on 2017/1/23.
 */
@SuppressWarnings("serial")
public class InvalidParameterException extends ApiException {

	public InvalidParameterException(String msg) {
		super(msg);
	}
	
}
