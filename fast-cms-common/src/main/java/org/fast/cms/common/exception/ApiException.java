package org.fast.cms.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by lenovo on 2017/1/23.
 */
@Getter
@Setter
@SuppressWarnings("serial")
public class ApiException extends RuntimeException {

    private String msg;

    public ApiException(String msg) {
        super(msg);
        this.msg = msg;
    }

    public ApiException(String msg, Throwable e) {
        super(msg, e);
        this.msg = msg;
    }

}
