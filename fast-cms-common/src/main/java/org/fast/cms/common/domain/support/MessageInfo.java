package org.fast.cms.common.domain.support;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月18日 上午7:29:35 
*/
@SuppressWarnings("serial")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MessageInfo implements Serializable {

    private String code;

    private String message;
}
