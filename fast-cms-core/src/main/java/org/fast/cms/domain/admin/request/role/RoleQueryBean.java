package org.fast.cms.domain.admin.request.role;

import org.fast.cms.common.domain.request.AbstractQueryParameterBean;

import lombok.Getter;
import lombok.Setter;

/** 
* 
* @author weigen.ye 
* @date 创建时间：2017年7月11日 上午9:34:32 
*/
@Getter
@Setter
@SuppressWarnings("serial")
public class RoleQueryBean extends AbstractQueryParameterBean{
	private String roleName;
}
