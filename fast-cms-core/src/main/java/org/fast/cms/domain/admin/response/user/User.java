package org.fast.cms.domain.admin.response.user;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.fast.cms.domain.admin.response.menu.Menu;

import lombok.Getter;
import lombok.Setter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午12:54:12 
*
*/
@Getter
@Setter
@SuppressWarnings("serial")
public class User implements Serializable {

	/** 用户code */
    private Integer userId;

    /** 登录名 **/
    private String loginName;
    
    /** 姓名 */
    private String userName;

    /** 电子邮件 */
    private String email;

    /** 手机号码 */
    private String mobile;

    /** 账户激活标志，0：未冻结，1：冻结  */
    private Integer isActivated;
    
    /** 账户冻结标志，0：未冻结，1：冻结  */
    private Integer isFrozen;
    
    /**
     * 是否管理员
     */
    private boolean isAdmin;

    /**
     * 数据权限范围
     */
    private int dataScope;
    
    /**
     * 可操作菜单列表（包含子菜单、菜单动作）
     */
    private List<Menu> menuList;
    
    /**
     * 可操作列表集合
     */
    private Map<String, Integer> permission;
    
    /**
     * API 列表集合
     */
    private Set<String> apiList;
    
    private Integer roleType;
}
