package org.fast.cms.domain.admin.bean;

import org.fast.cms.entity.admin.SysRole;

import lombok.Getter;
import lombok.Setter;

/** 
* 
* @author weigen.ye 
* @date 创建时间：2017年7月11日 上午9:41:18 
*/
@Getter
@Setter
@SuppressWarnings("serial")
public class RoleListBean extends SysRole{

	/**
	 * 角色类型名
	 */
	private String roleTypeName;
}
