package org.fast.cms.domain.admin.response.menu;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午12:56:14 
*
*/
@Getter
@Setter
@SuppressWarnings("serial")
public class Menu implements Serializable {

    /** 菜单Code */
    private int menuCode;

    /** 菜单名称*/
    private String menuName;

    /** 菜单链接 */
    private String href;

    /** 菜单图标 */
    private String icon;

    /** 显示标志 */
    private Integer displayFalg;

    /** 子菜单列表 */
    private List<Menu> childMenu;
    
    /** 权限字符串 */
    private String permission;
}
