package org.fast.cms.domain.admin.response.role;

import org.fast.cms.common.domain.response.AbstractPageBean;
import org.fast.cms.domain.admin.bean.RoleListBean;

import lombok.Getter;
import lombok.Setter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年7月11日 上午9:40:01 
*
*/
@Getter
@Setter
@SuppressWarnings("serial")
public class RoleResponseBean extends AbstractPageBean<RoleListBean>{

}
