package org.fast.cms.persistent.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.fast.cms.domain.admin.bean.RoleListBean;
import org.fast.cms.entity.admin.SysMenu;
import org.fast.cms.entity.admin.SysRole;

import com.github.pagehelper.Page;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午2:43:10 
*
*/
public interface SystemCommonMapper {
	
	/**
	 * 取得用户可操作菜单、动作
	 * @param userId
	 * @return
	 */
	public List<SysMenu> getMenuActionByUserId(@Param("userId") int userId);
	
	/**
	 * 取得用户可操作菜单、动作
	 * @param userId
	 * @return
	 */
	public List<SysMenu> getAllMenuAction();
	
	/**
	 * 取得用户角色
	 * @param userId
	 * @return
	 */
	public List<SysRole> getUserRole(@Param("userId") int userId);
	
	/**
	 * 
	 * @param roleName
	 * @return
	 */
	Page<RoleListBean> getRoleList(@Param("roleName") String roleName);
	
	/**
	 * 根据角色ID得到对应目录ID
	 * @param roleCode 角色ID
	 * @return
	 */
	List<Integer> getMenuCodeByRole(@Param("roleCode") Integer roleCode);

}
