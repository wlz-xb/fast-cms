package org.fast.cms.controller.admin;

import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.fast.cms.common.core.AbstractController;
import org.fast.cms.common.domain.response.JsonResponse;
import org.fast.cms.common.exception.BusinessException;
import org.fast.cms.domain.admin.request.role.RoleQueryBean;
import org.fast.cms.domain.admin.response.role.RoleResponseBean;
import org.fast.cms.service.admin.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/admin/role")
public class RoleController extends AbstractController{

	@Autowired
	private SysRoleService roleService;
	
	/**
	 * 角色列表
	 * @return
	 */
	@RequiresPermissions("role:list")
	@RequestMapping(value="/list", method=RequestMethod.GET)
	public JsonResponse<RoleResponseBean> list(RoleQueryBean bean) {
		if (bean == null || bean.getPageNum() == null || bean.getPageSize() == null) {
			throw new BusinessException("没有分页信息");
		}
		return success(roleService.list(bean));
	}

	/**
	 * 取得角色菜单
	 * @param roleCode
	 * @return
	 */
//	@RequiresPermissions("role:permission:add")
	@RequestMapping(value="menu/list", method=RequestMethod.GET)
	public JsonResponse<List<Integer>> menuList(@RequestParam(value="roleCode", required=true) Integer roleCode) {
		return success(roleService.getMenulistByRole(roleCode));
	}
}

