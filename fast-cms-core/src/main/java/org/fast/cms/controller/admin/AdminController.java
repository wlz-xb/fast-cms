package org.fast.cms.controller.admin;

import org.fast.cms.common.core.AbstractController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月25日 下午1:40:43 
*
*/
@Controller
@RequestMapping(value="/admin")
public class AdminController extends AbstractController{
	
	
	@RequestMapping(value="/index")
	public  String index(){
		return getAdminTemplate("index");
	}
}
