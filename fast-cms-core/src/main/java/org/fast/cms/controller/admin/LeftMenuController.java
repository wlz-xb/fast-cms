package org.fast.cms.controller.admin;

import java.util.List;

import org.fast.cms.common.core.AbstractController;
import org.fast.cms.common.domain.response.JsonResponse;
import org.fast.cms.domain.admin.response.menu.Menu;
import org.fast.cms.utils.UserUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/** 
* 
* 获取对应用户的管理后台左则目录栏信息
* @author weigen.ye 
* @date 创建时间：2017年7月7日 下午12:33:40 
*/
@RestController
@RequestMapping(value="/admin")
public class LeftMenuController extends AbstractController{
    
	@RequestMapping(value="/leftMenuTree", method=RequestMethod.GET)
	public JsonResponse<List<Menu>> getLeftMenuTree() {
		return success(UserUtils.getMenuTree());
	}
}
