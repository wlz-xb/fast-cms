package org.fast.cms.enums;
/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午3:10:20 
*
*/
public enum BusinessTypeEnum {

	AGENT(1,"代理商"),
	CLUE(2, "线索"),
	NICHE(3, "机会"),
	CUSTOMER(4,"客户"),
	CONTRACT(5, "合同"),
	BUSINESS(6,"订单"),
	PAY(7, "回款"),
	SUPPORT_TASK(8, "客服支持"),
	DELIVER_TASK(9,"硬件订单发货"),
	STAFF(10, "员工"),
	REPORT(11, "报表");
	
	private final int key;
	private final String value;
	
	private BusinessTypeEnum(int key, String value) {
		this.key = key;
		this.value = value;
	}

	public int getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}
}
