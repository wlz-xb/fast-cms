package org.fast.cms.service.admin;

import org.fast.cms.entity.admin.SysUser;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午1:13:28 
*
*/
public interface SysUserService {
    
	/**
	 * 根据Key查找用户
	 * @param userId
	 * @return
	 */
	public SysUser getUserById(Integer userId);
	
	/**
	 * 通过用户名得到用户id
	 * @param userName
	 * @return
	 */
	public Integer[] getUserIdByName(String userName);
	
	/**
	 * 根据登录得到用户
	 * @param loginName
	 * @return
	 */
	public SysUser getUserByLoginName(String loginName);
}
