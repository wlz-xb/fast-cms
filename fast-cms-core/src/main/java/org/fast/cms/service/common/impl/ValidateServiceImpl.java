package org.fast.cms.service.common.impl;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.fast.cms.common.domain.response.ImgData;
import org.fast.cms.common.domain.response.ValidateCodeData;
import org.fast.cms.service.common.ValidateService;
import org.springframework.stereotype.Service;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月18日 上午7:22:40 
*
*/
@Service
public class ValidateServiceImpl implements ValidateService{

	private final int w = 120;
    private final int h = 45;
    
	/**
     * 生成验证码
     * @param request
     * @return
     */
    public ImgData getValidateCodeImg(HttpServletRequest request) {

        ValidateCodeData d = new ValidateCodeData();
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();

		/*
		 * 生成背景
		 */
        createBackground(g);

		/*
		 * 生成字符
		 */
        String s = createCharacter(g);
        d.setValidateCode(s);
        g.dispose();

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "JPEG", out);
            byte[] buffer = out.toByteArray();
            String data = Base64.encodeBase64String(buffer);
            d.setType("image/jpeg");
            d.setBase64(data);
            return d;
        } catch (IOException e) {
            return new ImgData();
        }
    }

    private Color getRandColor(int fc, int bc) {
        int f = fc;
        int b = bc;
        Random random=new Random();
        if(f>255) {
            f=255;
        }
        if(b>255) {
            b=255;
        }
        return new Color(f+random.nextInt(b-f),f+random.nextInt(b-f),f+random.nextInt(b-f));
    }

    private void createBackground(Graphics g) {
        // 填充背景
        g.setColor(getRandColor(220,250));
        g.fillRect(0, 0, w, h);
        // 加入干扰线条
        for (int i = 0; i < 8; i++) {
            g.setColor(getRandColor(40,150));
            Random random = new Random();
            int x = random.nextInt(w);
            int y = random.nextInt(h);
            int x1 = random.nextInt(w);
            int y1 = random.nextInt(h);
            g.drawLine(x, y, x1, y1);
        }
    }

    private String createCharacter(Graphics g) {
        char[] codeSeq = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J',
                'K', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
                'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9' };
        String[] fontTypes = {"Arial","Arial Black","AvantGarde Bk BT","Calibri"};
        Random random = new Random();
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 5; i++) {
            String r = String.valueOf(codeSeq[random.nextInt(codeSeq.length)]);//random.nextInt(10));
            g.setColor(new Color(50 + random.nextInt(100), 50 + random.nextInt(100), 50 + random.nextInt(100)));
            g.setFont(new Font(fontTypes[random.nextInt(fontTypes.length)],Font.BOLD,28));
            g.drawString(r, 20 * i + 10, 25 + random.nextInt(8));
//			g.drawString(r, i*w/4, h-5);
            s.append(r);
        }
        return s.toString();
    }

}
