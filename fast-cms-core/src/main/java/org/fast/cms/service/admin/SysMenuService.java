package org.fast.cms.service.admin;

import java.util.List;

import org.fast.cms.domain.admin.response.menu.Menu;
import org.fast.cms.entity.admin.SysMenu;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午1:13:41 
*
*/
public interface SysMenuService {

	 /**
     * 数据库Menu转换为Tree
     * @param menuList
     * @return
     */
	public List<Menu> getMenuTree(List<SysMenu> menuList);
    
    /**
     * 取得用户菜单/操作列表
     * @param userId
     * @return
     */
    public List<SysMenu> getMenuActionByUserId(Integer userId);
    
    /**
     * 取得所有菜单/操作列表
     * @return
     */
    public List<SysMenu> getAllMenuAction();
}
