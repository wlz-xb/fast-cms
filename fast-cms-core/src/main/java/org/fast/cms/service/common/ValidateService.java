package org.fast.cms.service.common;

import javax.servlet.http.HttpServletRequest;

import org.fast.cms.common.domain.response.ImgData;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月18日 上午7:20:39 
*
*/
public interface ValidateService {

	public static final String VALIDATE_CODE = "validateCode";

    /**
     * 生成验证码
     * @param req
     * @return
     */
    ImgData getValidateCodeImg(HttpServletRequest request);
}
