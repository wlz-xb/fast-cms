package org.fast.cms.service.admin;

import java.util.List;

import org.fast.cms.domain.admin.request.role.RoleQueryBean;
import org.fast.cms.domain.admin.response.role.RoleResponseBean;
import org.fast.cms.entity.admin.SysRole;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午1:13:52 
*
*/
public interface SysRoleService {
	
	/**
	 * 查询
	 * @param roleName
	 * @return
	 */
	RoleResponseBean list(RoleQueryBean bean);
	
	/**
	 * 查找用户角色
	 * @param userId
	 * @return
	 */
	List<SysRole> getUserRole(Integer userId);
	
	/**
	 * 获取角色对应的目录ID
	 * @param roleCode
	 * @return
	 */
	List<Integer> getMenulistByRole(Integer roleCode);

}
