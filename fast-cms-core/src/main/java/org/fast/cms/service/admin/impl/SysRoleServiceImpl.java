package org.fast.cms.service.admin.impl;

import java.util.List;

import org.fast.cms.common.utils.CommonUtils;
import org.fast.cms.domain.admin.bean.RoleListBean;
import org.fast.cms.domain.admin.request.role.RoleQueryBean;
import org.fast.cms.domain.admin.response.role.RoleResponseBean;
import org.fast.cms.entity.admin.SysRole;
import org.fast.cms.persistent.mapper.common.SystemCommonMapper;
import org.fast.cms.service.admin.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午3:05:09 
*
*/
@Service
public class SysRoleServiceImpl implements SysRoleService{

	@Autowired
    private SystemCommonMapper sysComMapper;
	
	@Override
	public List<SysRole> getUserRole(Integer userId) {
		return sysComMapper.getUserRole(userId);
	}

	/**
     * 取得角色列表
     */
	@Override
	public RoleResponseBean list(RoleQueryBean bean) {
		PageHelper.startPage(bean.getPageNum(), bean.getPageSize());
		Page<RoleListBean> list = sysComMapper.getRoleList(bean.getRoleName());
		RoleResponseBean result = new RoleResponseBean();
    	CommonUtils.setPageInfo(result, list);
    	return result;
	}

	@Override
	public List<Integer> getMenulistByRole(Integer roleCode) {
		return sysComMapper.getMenuCodeByRole(roleCode);
	}

}
