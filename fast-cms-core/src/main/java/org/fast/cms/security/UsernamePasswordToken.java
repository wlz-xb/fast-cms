package org.fast.cms.security;

import lombok.Getter;
import lombok.Setter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午12:37:15 
*
*/

@Getter
@Setter
@SuppressWarnings("serial")
public class UsernamePasswordToken extends org.apache.shiro.authc.UsernamePasswordToken {
	
	private String validateCode;

	/**
	 *
	 * @param userName
	 * @param password
	 * @param validateCode
	 * @param rememberMe
	 */
	public UsernamePasswordToken(String userName, String password,
		   String validateCode, boolean rememberMe) {
		this.setUsername(userName);
		this.setPassword(password != null ? password.toCharArray() : null);
		this.setValidateCode(validateCode);
		this.setRememberMe(rememberMe);
	}
}
