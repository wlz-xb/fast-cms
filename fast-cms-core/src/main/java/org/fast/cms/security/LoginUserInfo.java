package org.fast.cms.security;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by junxiao on 2017/1/1.
 */
@Getter
@Setter
@AllArgsConstructor
@SuppressWarnings("serial")
public class LoginUserInfo implements Serializable {

	private Integer userId;
	
    private String loginName;
    
}
