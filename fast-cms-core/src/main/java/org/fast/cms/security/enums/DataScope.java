package org.fast.cms.security.enums;

/**
 * 数据范围
 * @author lenovo
 *
 */
public enum DataScope {

	ALL(10, "所有数据"),
	COMPANY_ALL_DATA(20, "所在公司及以下数据"),
	CONPANY_DATA(30, "所在公司数据"),
	DEPARTMENT_ALL_DATA(40, "所在部门及下级部门数据"),
	DEPARTMENT_DATA(50, "所在部门数据"),
	OWN_DATA(90, "仅本人的数据"),
	BIZ_SET_DATA(100, "按照明细设置");
	
	private final int type;
	private String despt;
	private DataScope(int type, String despt) {
		this.type = type;
		this.despt = despt;
	}
	public int getType() {
		return type;
	}
	public String getDespt() {
		return despt;
	}
	
	// 根据key 查询枚举类
	public static DataScope getByKey(int key) {
        for (DataScope dataScope : DataScope.values()) {
            if (dataScope.getType() == key) {
                return dataScope;
            }
        }

        return null;
    }
	
}
