package org.fast.cms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

/** 
* @author weigen.ye 
* @date 创建时间：2017年6月17日 下午12:44:29 
*
*/
@Getter
@Setter
@Component
public class AdminAppProperties {
	
	@Value("${default.password}")
	private String defaultPassword;
    
    @Value("${crm.password}")
    private String password;
    
    @Value("${role.admin}")
    private String adminName;

    @Value("${validate.used}")
    private boolean validateCodeUsed;

}
