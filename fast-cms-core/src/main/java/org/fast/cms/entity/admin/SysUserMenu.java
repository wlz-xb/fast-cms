package org.fast.cms.entity.admin;

public class SysUserMenu {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_user_menu.user_code
     *
     * @mbggenerated
     */
    private Integer userCode;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column sys_user_menu.menu_code
     *
     * @mbggenerated
     */
    private Integer menuCode;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_user_menu.user_code
     *
     * @return the value of sys_user_menu.user_code
     *
     * @mbggenerated
     */
    public Integer getUserCode() {
        return userCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_user_menu.user_code
     *
     * @param userCode the value for sys_user_menu.user_code
     *
     * @mbggenerated
     */
    public void setUserCode(Integer userCode) {
        this.userCode = userCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column sys_user_menu.menu_code
     *
     * @return the value of sys_user_menu.menu_code
     *
     * @mbggenerated
     */
    public Integer getMenuCode() {
        return menuCode;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column sys_user_menu.menu_code
     *
     * @param menuCode the value for sys_user_menu.menu_code
     *
     * @mbggenerated
     */
    public void setMenuCode(Integer menuCode) {
        this.menuCode = menuCode;
    }
}