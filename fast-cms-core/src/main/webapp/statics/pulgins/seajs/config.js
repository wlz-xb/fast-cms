/**
 * 使用的js路径配置
 */
seajs.config({
	base :ctx+'/statics/',
	alias : {
		'common' : 'common/js/common',
		'adminCustom' : 'admin/js/adminCustom',
		'leftMenu':'admin/js/leftMenu',
		'adminLogin' : 'admin/js/login',
		'jquery':'pulgins/jquery/dist/jquery.min',
		'bootstrap':'pulgins/bootstrap/dist/js/bootstrap.min',
		'layer':'pulgins/layer/layer',
		'validate':'pulgins/validate/1.12.0/jquery.validate.min',
		'jform':'pulgins/form/3.50.0/jquery.form',
		'template':'pulgins/template/template',
		'upload':'pulgins/upload/ajaxupload',
		'cookie':'pulgins/cookie/jquery.cookie',
	}
});