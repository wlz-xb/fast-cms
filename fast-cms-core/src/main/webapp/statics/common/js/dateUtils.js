var dateUtils =(function() {
	/**
	 * 日期转换
	 * @param date Date 
	 * @param fmt 日期格式化字符串
	 */
    var dateFormat=function(date,fmt) { 
            var o = { 
                "M+" : date.getMonth()+1,                 //月份 
                "d+" : date.getDate(),                    //日 
                "h+" : date.getHours(),                   //小时 
                "m+" : date.getMinutes(),                 //分 
                "s+" : date.getSeconds(),                 //秒 
                "q+" : Math.floor((date.getMonth()+3)/3), //季度 
                "S"  : date.getMilliseconds()             //毫秒 
            }; 
            if(/(y+)/.test(fmt)) {
                    fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length)); 
            }
            for(var k in o) {
            if(new RegExp("("+ k +")").test(fmt)){
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
                }
            }
        return fmt; 
    }; 

    /**
     *  将日期字符串转换成Date
     *  @param dateStr
     */
    var parserDate = function (dateStr) { 
        var t = Date.parse(dateStr);  
        if (!isNaN(t)) {  
            return new Date(Date.parse(dateStr.replace(/-/g, "/")));  
        } else {  
            return new Date();  
        }  
    }; 

    /**
    *  对应日期上增加年月日
    *  @param date Date 
    *  @param part y年， m月， d日， h小时， n分钟，s秒  
    *  @param 对应值
    */
    var dateAdd = function (date,part,value) {  
        value *= 1;  
        if (isNaN(value)) {  
            value = 0;  
        }  
        switch (part) {  
            case "y":  
                date.setFullYear(date.getFullYear() + value);  
                break;  
            case "m":  
                date.setMonth(date.getMonth() + value);  
                break;  
            case "d":  
                date.setDate(date.getDate() + value);  
                break;  
            case "h":  
                date.setHours(date.getHours() + value);  
                break;  
            case "n":  
                date.setMinutes(date.getMinutes() + value);  
                break;  
            case "s":  
                date.setSeconds(date.getSeconds() + value);  
                break;  
            default:  
    
        } 
        return date; 
    }  
    
    return {
        dateFormat: dateFormat,
        dateAdd:dateAdd,
        parserDate:parserDate
    }
})();