/**
 * 登录校验与验证码获取
 */
define(function(require, exports, module) {
	var $ = require('jquery');
	var common = require('common');
	var cookie = require('cookie');
	
	var init = function() {
		$("#login-submit").click(function () {
	        // 如果选中设为true
	        var rememberId_c = $('#rememberId').is(':checked');
	        if (rememberId_c) {
	            var username_co = $('#username').val();
	            var rememberId_co = $('#rememberId').is(':checked');
	            $.cookie('username', username_co, { expires: 7 });
//	            $.cookie('rememberMe', rememberId_co, { expires: 7 });
	        } else {
	            $.cookie('username', '', { expires: -1 });
//	            $.cookie('rememberMe', '', { expires: -1 });
	        }

	        var userName = $("#username").val();
	        var datas = {};
	        if ($("#username").val() == "" || $("#password").val() == "" || $("#username").val() == "请输登录账号" || $("#password").val() == "请输密码") {
	            $("#emptyInfo").fadeIn("slow");
	            return;
	        }
	       common.submitForm("#loginForm", ctx+'/admin/realLogin', function() {
	           window.location.assign(ctx+'/admin/index');
	       }, null, function() {
	            if ($("#validateImg").length > 0) {
	                $("#validateImg").attr('src', ctx+'/validateCode?d=' + (new Date()));
	            }
	       });

	    });
		
		//初始化验证码
		$("#validateImg").attr('src', ctx+'/validateCode?d=' + (new Date()));
		
	    //点击时生成验证码
	    $("#validateImg").click(function() {
	        $("#validateImg").attr('src', ctx+'/validateCode?d=' + (new Date()));
	    });
	    
	    $(document).keypress(function (e) {
	        // 回车键事件  
	        if (e.which == 13) {
	            $("#login-submit").click();
	        }
	    }); 	    
	};
	
	return {
		init : init
	}
});