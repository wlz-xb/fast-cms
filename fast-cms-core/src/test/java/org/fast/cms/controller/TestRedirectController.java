package org.fast.cms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/** 
* 
* http://www.cnblogs.com/fangjian0423/p/springMVC-redirectView-analysis.html
* @author weigen.ye 
* @date 创建时间：2017年7月9日 下午1:28:21 
*
*/
@Controller
@RequestMapping(value = "/redirect")
public class TestRedirectController {
	
    @RequestMapping("/test1")
    public ModelAndView test1(ModelAndView view) {
    	//访问fast-cms/redirect/index
        view.setViewName("redirect:index");
        return view;
    }

    @RequestMapping("/test2")
    public ModelAndView test2(ModelAndView view) {
    	//访问fast-cms/redirect/login
        view.setViewName("redirect:login");
        return view;
    }
    
    @RequestMapping("/test3")
    public ModelAndView test3(ModelAndView view) {
    	//访问fast-cms/index
        view.setViewName("redirect:/index");
        return view;
    }

    @RequestMapping("/test4")
    public ModelAndView test4(ModelAndView view) {
        view.setView(new RedirectView("/index", false));
        return view;
    }

    @RequestMapping("/test5")
    public ModelAndView test5(ModelAndView view) {
        view.setView(new RedirectView("index", false));
        return view;
    }

    @RequestMapping("/test6/{id}")
    public ModelAndView test6(ModelAndView view, @PathVariable("id") int id) {
    	view.addObject("test", "test");
        view.setViewName("redirect:/index{id}");
        return view; 
    }
    
    @RequestMapping("/test7/{id}")
    public ModelAndView test7(ModelAndView view, @PathVariable("id") int id) {
        RedirectView redirectView = new RedirectView("/index{id}");
        //重定向不替换{id}
        redirectView.setExpandUriTemplateVariables(false);
        //重定向不传递属性
        redirectView.setExposeModelAttributes(false);
        view.setView(redirectView);
        view.addObject("test", "test");
        return view;
    }
  
}
